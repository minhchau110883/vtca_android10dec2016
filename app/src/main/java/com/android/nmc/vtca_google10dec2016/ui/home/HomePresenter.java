package com.android.nmc.vtca_google10dec2016.ui.home;

/**
 * Created by cnguyen43 on 12/10/2016.
 */

public class HomePresenter implements HomeContract.Presenter {

    HomeContract.View mView;

    private int mValue;


    @Override
    public void increaseValue() {
        mValue++;
        mView.UpdateValue(String.valueOf(mValue));
    }

    @Override
    public void setView(HomeContract.View view) {
        mView = view;
    }
}
