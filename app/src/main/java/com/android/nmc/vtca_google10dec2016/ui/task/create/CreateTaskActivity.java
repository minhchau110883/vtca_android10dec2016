package com.android.nmc.vtca_google10dec2016.ui.task.create;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.android.nmc.vtca_google10dec2016.R;
import com.android.nmc.vtca_google10dec2016.data.domain.Task;
import com.android.nmc.vtca_google10dec2016.data.source.TasksDataSource;
import com.android.nmc.vtca_google10dec2016.data.source.TasksRepository;
import com.android.nmc.vtca_google10dec2016.data.source.local.TasksLocalDataSource;
import com.android.nmc.vtca_google10dec2016.ui.base.BaseActivity;
import com.android.nmc.vtca_google10dec2016.ui.task.list.TaskListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateTaskActivity extends BaseActivity implements CreateTaskContract.View{

    public static Intent newIntent(Context context) {
        return new Intent(context, CreateTaskActivity.class);
    }

    private CreateTaskPresenter mPresenter;
    private TasksDataSource tasksRepository;

    @BindView(R.id.saveTaskBtn)
    protected FloatingActionButton mSaveBtn;

    @BindView(R.id.add_task_title)
    protected EditText mTitle;

    @BindView(R.id.add_task_description)
    protected EditText mDescription;

    @Override
    public void initializePresenter() {
        tasksRepository = TasksRepository.getInstance(TasksLocalDataSource.getInstance(getApplicationContext()),
                TasksLocalDataSource.getInstance(getApplicationContext()));

        mPresenter = new CreateTaskPresenter(null, tasksRepository);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_task);

        ButterKnife.bind(this);

        mPresenter.setView(this);

        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = mTitle.getText().toString();
                String desc = mDescription.getText().toString();
                System.out.println("========Title: " + title + "========Desc: "  + desc);
                mPresenter.saveTask(title, desc);
            }
        });
    }
}
