package com.android.nmc.vtca_google10dec2016.data.source.local;

import android.provider.BaseColumns;

/**
 * Created by cnguyen43 on 12/10/2016.
 */

public class TasksPersistenceContract {

    private TasksPersistenceContract() {}

    public static abstract class TaskEntry implements BaseColumns {
        public static final String TABLE_NAME = "task";
        public static final String COLUMN_NAME_ENTRY_ID = "entryid";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_COMPLETED = "completed";
    }
}
