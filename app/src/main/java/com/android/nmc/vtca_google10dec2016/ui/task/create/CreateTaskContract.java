package com.android.nmc.vtca_google10dec2016.ui.task.create;

import com.android.nmc.vtca_google10dec2016.ui.base.BasePresenter;
import com.android.nmc.vtca_google10dec2016.ui.base.BaseView;

/**
 * Created by cnguyen43 on 12/10/2016.
 */

public class CreateTaskContract {

    public interface View extends BaseView {

    }

    public interface Presenter extends BasePresenter<View> {
        public void saveTask(String title, String desc);


    }
}
