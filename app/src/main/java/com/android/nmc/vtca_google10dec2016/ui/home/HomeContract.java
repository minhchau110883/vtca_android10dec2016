package com.android.nmc.vtca_google10dec2016.ui.home;

import com.android.nmc.vtca_google10dec2016.ui.base.BasePresenter;
import com.android.nmc.vtca_google10dec2016.ui.base.BaseView;

/**
 * Created by cnguyen43 on 12/10/2016.
 */

public interface HomeContract {

    public interface View extends BaseView {
        void UpdateValue(String value);
    }

    public interface Presenter extends BasePresenter<View> {
        void increaseValue();
    }
}
