package com.android.nmc.vtca_google10dec2016.ui.home;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.nmc.vtca_google10dec2016.R;
import com.android.nmc.vtca_google10dec2016.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements HomeContract.View{

    public static Intent newIntent(Context context) {
        return new Intent(context, HomeActivity.class);
    }

    @BindView(R.id.mBtnIncrease)
    protected Button mBtnIncrease;

    HomePresenter mPresenter;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        mPresenter = new HomePresenter();
        mPresenter.setView(this);

        mBtnIncrease.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mPresenter.increaseValue();
            }
        });

    }

    @Override
    public void initializePresenter() {
        setContentView(R.layout.activity_home);
    }


    @Override
    public void UpdateValue(String value) {
        mBtnIncrease.setText(value);
    }
}
