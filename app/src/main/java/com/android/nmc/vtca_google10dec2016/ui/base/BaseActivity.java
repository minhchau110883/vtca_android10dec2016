package com.android.nmc.vtca_google10dec2016.ui.base;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.nmc.vtca_google10dec2016.R;

public abstract class BaseActivity extends AppCompatActivity {

    public abstract void initializePresenter();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializePresenter();
    }


}
