package com.android.nmc.vtca_google10dec2016.ui.task.create;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.android.nmc.vtca_google10dec2016.data.domain.Task;
import com.android.nmc.vtca_google10dec2016.data.source.TasksDataSource;
import com.android.nmc.vtca_google10dec2016.ui.home.HomeContract;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by cnguyen43 on 12/10/2016.
 */

public class CreateTaskPresenter implements CreateTaskContract.Presenter {

    @NonNull
    CreateTaskContract.View mView;

    @NonNull
    private final TasksDataSource mTasksRepository;
    @Nullable
    private String mTaskId;

    public CreateTaskPresenter(@Nullable String taskId, @NonNull TasksDataSource tasksRepository) {
        mTaskId = taskId;
        mTasksRepository = checkNotNull(tasksRepository);
    }


    @Override
    public void saveTask(String title, String desc) {
        createTask(title, desc);
    }

    @Override
    public void setView(CreateTaskContract.View view) {
        mView = view;
    }

    private void createTask(String title, String desc) {
        Task newTask = new Task(title, desc);
        if (newTask.isEmpty()) {
            //mView.showEmptyTaskError();
        } else {
            mTasksRepository.saveTask(newTask);
           // mView.showTasksList();
            mTasksRepository.getTasks(new TasksDataSource.LoadTasksCallback(){

                @Override
                public void onTasksLoaded(List<Task> tasks) {
                    System.out.print("Tasks size: " + tasks.size());
                }

                @Override
                public void onDataNotAvailable() {
                    System.out.print("Data not available");
                }
            });
        }
    }
}
