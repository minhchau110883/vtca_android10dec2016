package com.android.nmc.vtca_google10dec2016;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.nmc.vtca_google10dec2016.ui.base.BaseActivity;
import com.android.nmc.vtca_google10dec2016.ui.home.HomeActivity;
import com.android.nmc.vtca_google10dec2016.ui.task.list.TaskListActivity;

public class MainActivity extends BaseActivity {

    @Override
    public void initializePresenter() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        startActivity(HomeActivity.newIntent(this));
        startActivity(TaskListActivity.newIntent(this));
        finish();
    }
}
