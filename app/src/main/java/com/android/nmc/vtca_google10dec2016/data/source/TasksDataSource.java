package com.android.nmc.vtca_google10dec2016.data.source;

import android.support.annotation.NonNull;

import com.android.nmc.vtca_google10dec2016.data.domain.Task;

import java.util.List;

/**
 * Created by cnguyen43 on 12/10/2016.
 */

public interface TasksDataSource {


    interface LoadTasksCallback {
        void onTasksLoaded(List<Task> tasks);
        void onDataNotAvailable();
    }

    interface GetTaskCallback {
        void onTaskLoaded(Task task);
        void onDataNotAvailable();
    }

    void getTasks(@NonNull LoadTasksCallback callback);
    void getTask(@NonNull String taskId, @NonNull GetTaskCallback callback);
    public void saveTask(@NonNull Task task);
}
