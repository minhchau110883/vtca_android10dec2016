package com.android.nmc.vtca_google10dec2016.ui.task.list;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.android.nmc.vtca_google10dec2016.R;
import com.android.nmc.vtca_google10dec2016.ui.base.BaseActivity;
import com.android.nmc.vtca_google10dec2016.ui.home.HomeActivity;
import com.android.nmc.vtca_google10dec2016.ui.task.create.CreateTaskActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskListActivity extends BaseActivity {

    public static Intent newIntent(Context context) {
        return new Intent(context, TaskListActivity.class);
    }

    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @BindView(R.id.btnCreateTask)
    protected FloatingActionButton mCreateBtn;

    @Override
    public void initializePresenter() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        setSupportActionBar(mToolbar);

        ButterKnife.bind(this);

        mCreateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TaskListActivity.this, CreateTaskActivity.class);
                startActivity(intent);
            }
        });

    }

    public void createNewTask() {

    }
}
